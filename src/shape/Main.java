package shape;

import com.gabi.school.Employee;
import com.gabi.school.Student;
import com.gabi.school.Teacher;

public class Main {

    public static void main(String[] args) {

        Teacher t1 = new Teacher(
                1234567890567L,
                "Maria",
                "Popescu",
                46,
                "Engleză"
        );
        System.out.println(t1.toString());

        Student s1 = new Student(
                1237654323456L,
                "Ion",
                "Antonescu",
                22,
                8.7
        );
        System.out.println(s1.toString());


        Employee e1 = new Employee(
                24567898765L,
                "Dorian",
                "Dragomir",
                61,
                "Director"
        );
        System.out.println(e1.toString());



        Shape[] shapes =  new Shape[3];
        Triangle t = new Triangle(3, 52, "pink");
        shapes[0] = t;
        Square s = new Square(7);
        shapes[1] = s;
        Rectangle r = new Rectangle(5,8,"red");
        shapes[2] = r;
        for(Shape sh : shapes) {
            System.out.println(sh.toString());
        }

    }
}
