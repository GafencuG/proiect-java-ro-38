package shape;


public class Square extends Shape {

    public Square(int base) {
        super(base);
    }

    public Square(int base, String color) {
        super(base, color);
    }

    @Override
    public Integer getArea() {
        return base * base;
    }

    @Override
    public String toString() {
        return "I am a " + color + " square. My base is " + base + " and my area is " + getArea();
    }
}
