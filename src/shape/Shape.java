package shape;

public class Shape {

    public int base;
    public String color;

    public Shape(int base) {
        this.base = base;
        this.color = "black";
    }

    public Shape(int base, String color) {
        this.base = base;
        this.color = color;
    }

    public Integer getArea() {
        return null;
    }

    public String toString() {
        return "I am a " + color + "shape with base " + base;
    }
}

